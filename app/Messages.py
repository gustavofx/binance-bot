# -*- coding: UTF-8 -*-
# @yasinkuyu
from __future__ import print_function

class Messages():
    
    @staticmethod
    def get(action, msg):
        print (action + ' --> ' + msg)
        exit(1)