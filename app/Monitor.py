# -*- coding: UTF-8 -*-
# @gustavofx

# Define Python imports
from __future__ import print_function
import os
import time
import config 
import threading
import math


# Define Custom imports
from app.Database import Database
from app.Orders import Orders

class Monitor():

    # Define static vars
    WAIT_TIME_BUY_SELL = 1 # seconds
    WAIT_TIME_CHECK_BUY_SELL = 0.2 # seconds
    WAIT_TIME_CHECK_SELL = 5 # seconds
    WAIT_TIME_STOP_LOSS = 20 # seconds

    MAX_TRADE_SIZE = 7 # int

    def __init__(self, option):
        # Get argument parse options
        self.option = option

        # Define parser vars
        self.order_id = self.option.orderid
        self.quantity = self.option.quantity
        self.wait_time = self.option.wait_time
        self.stop_loss = self.option.stop_loss
        
        self.increasing = self.option.increasing
        self.decreasing = self.option.decreasing

        # BTC amount
        self.amount = self.option.amount

        self.monitoring = True

    def monitor(self, symbol):

        while (self.monitoring):
            try:
                startTime = time.time()

                # Fetches the ticker price
                lastPrice = Orders.get_ticker(symbol)
            
                # Order book prices
                lastBid, lastAsk = Orders.get_order_book(symbol)

                print( '%s, %s, %s' % (lastPrice, lastBid, lastAsk) )

                endTime = time.time()

                if endTime - startTime < self.wait_time:

                    time.sleep(self.wait_time - (endTime - startTime))
            except IOError as e:
                print (e.message)
    
    def inputWatcher(self):

        while True:
            input = raw_input()

            if (input == 'm'):
                self.monitoring = not self.monitoring

            if (input == 'b'):
                print("Efetuar compra")

            if(input == 'c'):
                exit(1)

    def run(self):
        
        symbol = self.option.symbol

        print ('\nBinance Monitor')
        print ('\nCoin: %s\n' % (symbol) )                        
                
        coinMonitor = threading.Thread(target=self.monitor, args=(symbol,))
        coinMonitor.start()

        inputWatcher = threading.Thread(target=self.inputWatcher, args=())
        inputWatcher.start()    
        


    
   